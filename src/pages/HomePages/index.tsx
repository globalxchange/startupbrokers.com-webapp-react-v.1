import HomeCover from '../../components/HomeCover';
import HomeFeature from '../../components/HomeFeature';
import NavbarLayout from '../../layouts/NavbarLayout';
import styles from './homePage.module.scss';

const HomePage = () => {
  return (
    <NavbarLayout>
      <HomeCover />
      <div className={styles.divider} />
      <HomeFeature />
    </NavbarLayout>
  );
};

export default HomePage;
