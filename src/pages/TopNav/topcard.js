import React, { useContext, useEffect, useRef } from "react";
import "./topcard.scss";
import "./topnav.scss";
// import CloseIcon from "../../static/images/toplogo/closeicon.svg"
// import CloseIcon from "../../static/images/toplogo/closeicon.svg"
// import CloseIcon from "../../static/images/toplogo/closeicon.svg"
import CloseIcon from "../../assets/images/toplogo/closeicon.svg"
import IndianOTCIcon from "../../assets/images/toplogo/indianotcicon.svg";
import TaxChainIcon from "../../assets/images/toplogo/taxchainicon.svg";
import IndianInvestorIcon from "../../assets/images/toplogo/indianinvestoricon.svg";
import IndiaCoinsIcon from "../../assets/images/toplogo/indiacoinsicon.svg";
import IndianMarketIcon from "../../assets/images/toplogo/metaverseicon.svg"
import BhararTrustIcon from "../../assets/images/toplogo/bharattrusticon.svg";
import InstaIcon from "../../assets/images/toplogo/instasvg.svg";
import DiscordIcon from "../../assets/images/toplogo/discordsvg.svg";
import YoutubeIcon from "../../assets/images/toplogo/youtubesvg.svg";
import EmailIcon from "../../assets/images/toplogo/mailsvg.svg";
import { MainContext } from "../Context/context";

function Topcard() {
    const value = useContext(MainContext);
    const wrapperRef = useRef();

    // useOutsideAlerter(wrapperRef);

    // function useOutsideAlerter(ref) {
    //     useEffect(() => {
    //         /**
    //          * Alert if clicked on outside of element
    //          */

    //         function handleClickOutside(event) {
    //             // console.log(ref.current, event.target, "kwjbfkwjbefc");
    //             if (ref.current && !ref.current.contains(event.target)) {
    //                 value.setopencard("translateY(-140%)")
    //                 value.setpagemask("")
    //                 value.setclosedisp("none")
    //                 value.settherightcard("translateY(-140%)")
    //                 value.setpagemask("")
    //                 value.setcloserightdisp("none")
    //                 value.seticondisp("none")
    //             }
    //         }
    //         document.addEventListener("mousedown", handleClickOutside);
    //         return () => {
    //             document.removeEventListener("mousedown", handleClickOutside);
    //         };
    //     }, [ref]);
    // }

    const closeoverlay = (e) => {
        value.setopencard("translateY(-140%)")
        value.setpagemask("")
        value.setclosedisp("none")
        value.settherightcard("translateY(-140%)")
        value.setpagemask("")
        value.setcloserightdisp("none")
    }

    let carddata = [
        {
            "image": IndianOTCIcon,
            "title": "IndianOTC",
            "desc": "Digital Asset Trading Hub"
        },
        {
            "image": TaxChainIcon,
            "title": "TaxChains",
            "desc": "Crypto Tax Software"
        },
        {
            "image": IndianInvestorIcon,
            "title": "IndianInvestor",
            "desc": "Wealth Management System"
        },
        {
            "image": IndiaCoinsIcon,
            "title": "IndiaCoins",
            "desc": "India`s NFT Marketplace"
        },
        {
            "image": IndianMarketIcon,
            "title": "IndianMarket",
            "desc": "Financial Media & News"
        },
        {
            "image": BhararTrustIcon,
            "title": "BharatTrust",
            "desc": "Social Investing Platform"
        },
    ]

    let rightdata = [
        {
            "image": InstaIcon,
            "title": "Instagram",
            "desc": "@inr.group"
        },
        {
            "image": DiscordIcon,
            "title": "Discord",
            "desc": "Join Server"
        },
        {
            "image": YoutubeIcon,
            "title": "Youtube",
            "desc": "Go To Channel"
        },
        {
            "image": EmailIcon,
            "title": "Email",
            "desc": "support@inr.group"
        },
    ]
    const closemenu = (e) => {
        value.setopencard("translateY(-140%)")
        value.setpagemask("")
        value.setclosedisp("none")
    }


    const rightclose = (e) => {
        value.settherightcard("translateY(-140%)")
        value.setpagemask("")
        value.setcloserightdisp("none")
    }

    const opensite = (e) => {
        console.log(e.currentTarget.id)
        switch (e.currentTarget.id) {
            case "IndianOTC":
                window.open("https://indianotc.com", "_blank")
                break;
            case "TaxChains":
                window.open("https://taxchains.com", "_blank")
                break;
            case "IndianInvestor":
                window.open("https://indianinvestor.com", "_blank")
                break;
            case "IndiaCoins":
                window.open("https://indiacoins.com", "_blank")
                break;
            case "IndianMarket":
                window.open("https://indian.market", "_blank")
                break;
            case "BharatTrust":
                window.open("https://bharattrust.com", "_blank")
                break;
        }

    }
    return (
        <>
            <img src={CloseIcon} alt="close-icon" className='close-img' onClick={closemenu} style={{ display: value.closedisp }} />
            <div className={value.pagemask} onClick={closeoverlay}></div>
            <div className='sidemenu' style={{ transform: value.opencard }}>
                <div className='side-text'>
                    {/* <img src={CloseIcon} alt="close-icon" className='close-img' onClick={closemenu} style={{ display: value.closedisp }} /> */}
                    <p className="card-title">One Account. Mutiple Advantages</p>
                    <p className='card-desc'>With An IndianOTC Account You Also Get Access To The INR Group Apps</p>
                    {carddata.map(e => {
                        return (
                            <>
                                <br />
                                <div className='card-data' id={e.title} onClick={opensite}>
                                    <img src={e.image} alt="indian-otc" className='company-logo' />
                                    <div className='card-text1'>
                                        <p className='comp-title'>{e.title}</p>
                                        <p className='comp-desc'>{e.desc}</p>
                                    </div>
                                </div>
                            </>
                        )
                    })}
                </div>
            </div>
            {/* rightmenu */}
            <img src={CloseIcon} alt="close-icon" className='close-img-right' onClick={rightclose} style={{ display: value.closerightdisp }} />
            <div className='rightmenu' style={{ transform: value.therightcard }} >
                <div className='side-text'>
                    <p className="right-card-title">Connect With Us</p>
                    <p className='right-card-desc'>We Would Love To Hear From You</p>
                    {rightdata.map(e => {
                        return (
                            <>
                                <br />
                                <div className='card-data'>
                                    <img src={e.image} alt="indian-otc" className='company-logo' />
                                    <div className='card-text1'>
                                        <p className='comp-title'>{e.title}</p>
                                        <p className='comp-desc'>{e.desc}</p>
                                    </div>
                                </div>
                            </>
                        )
                    })}
                </div>
            </div>
        </>
    )
}



export default Topcard