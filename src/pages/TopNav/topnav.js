import React, { useContext } from "react";
import "./topnav.scss";
import "./topcard.scss";
import NvestIcon from "../../assets/images/toplogo/bosicon0.svg";
import DotsIcon from "../../assets/images/toplogo/groupdotsicon.svg";
import MailIcon from "../../assets/images/toplogo/mailicon.svg";
import IndIcon from "../../assets/images/toplogo/usicon1.svg";
import Topcard from "./topcard";
import { MainContext } from "../Context/context"


function Topnav() {
    const { setopencard, setpagemask, settherightcard, setclosedisp, setcloserightdisp } = useContext(MainContext)

    const openmenu = (e) => {
        setpagemask("pagemask")
        setopencard("translateY(0%)")
        setclosedisp("")
    }

    const openrightcard = (e) => {
        settherightcard("translateY(0%)")
        setpagemask("pagemask")
        setcloserightdisp("")
    }

    return (
        <div className='carres'>
            <img src={NvestIcon} alt="nvest-icon" className='left-icon' />
            <div className='vl-top'></div>
            <img src={DotsIcon} alt="dots-icon" className='left-icon' onClick={openmenu} />
            <div className='vl-top'></div>

            <div className='right-icons'>
                <div className='vl-top1'></div>
                <img src={MailIcon} alt="mail-icon" className='right-logo' onClick={openrightcard} />
                <div className='vl-top1'></div>
                <img src={IndIcon} alt="ind-icon" className='right-logo' />
            </div>
            <Topcard />
        </div>
    )
}

export default Topnav