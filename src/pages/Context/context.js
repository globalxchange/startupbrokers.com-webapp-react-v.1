import React, { Component, createContext, useState } from "react";

export const MainContext = createContext();

const MainContextProvider = ({ children }) => {
    const [opencard, setopencard] = useState("translateY(-140%)")
    const [therightcard, settherightcard] = useState("translateY(-140%)")
    const [closecard, setclosecard] = useState("")
    const [pagemask, setpagemask] = useState("")
    const [closedisp , setclosedisp] = useState("none")
    const [closerightdisp , setcloserightdisp] = useState("none")
    const value = {
        opencard,
        closecard,
        pagemask,
        therightcard,
        closedisp,
        closerightdisp,
        setopencard,
        setclosecard,
        setpagemask,
        settherightcard,
        setclosedisp,
        setcloserightdisp
    }
    return (
        <MainContext.Provider value={value}>
            {children}
        </MainContext.Provider>
    );
}

export default MainContextProvider;

export const MainContextConsumer = MainContext.Consumer