import { FC } from 'react';
import Navbar from '../../components/Navbar';
import Topnav from "../../pages/TopNav/topnav"
import styles from './navbarLayout.module.scss';

const NavbarLayout: FC = ({ children }) => {
  return (
    <div className={styles.navbarLayoutContainer}>
      <Topnav/>
      <Navbar />
      <div className={styles.childContainer}>{children}</div>
    </div>
  );
};

export default NavbarLayout;
