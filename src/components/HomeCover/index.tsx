import styles from './homeCover.module.scss';
import './home.scss';
import YoungWoman from "../../assets/images/companyLogos/womainicon1.png"
import directoryicon from "../../assets/images/companyLogos/directoryicon.svg"
import Searchicon from "../../assets/images/companyLogos/searchicon.svg"

const HomeCover = () => {
  return (
    <>
  <div className="row">
                <div className="col-sm-6">
                    <div className="woman-img">
                        <img src={YoungWoman} alt="young-woman" />
                    </div>
                </div>
                <div className="col-sm-6">
                    <p className="landing-title1">Find The Name That</p>
                    <p className="landing-title2">Best Suits Your Startup</p>
                    <br />
                    <p className="right-desc">VCFunnel Is The World's First Tool That Enables Startup's To Automate Their
                        Investor Relations Department.</p>
                    <br />
                    <div>
                        <button className="thebtn"><span><img src={directoryicon} alt="directory" /></span>&nbsp;Directory</button>
                        &nbsp;&nbsp;                        &nbsp;&nbsp;
                        <button className="thebtn"><span><img src={Searchicon} alt="directory" /></span>&nbsp;Deck Code</button>
                    </div>
                </div>
            </div>
            {/* <hr className = "hr-sty"/> */}
    {/* <div className={styles.coverContainer}>
      <div className={styles.thebgm}>
        <div className={styles.themaintext}>
          <p className= {styles.titletext}>Launching Your Startup?</p>
          <p className= {styles.desctext}>Find Everything You Need In One Place</p>
          <p className= {styles.descmbtext}>We Got You Covered</p>
          <button className= {styles.themainbtn}>Get Started</button>
        </div>
      </div> */}

      {/* <div className={styles.coverLeft}>
        <div className={styles.leftHeader}>
          We Have 100’s Of Exclusively Branded Startups That You Can Acquire Or
          Rent
        </div>
        <div className={styles.leftDesc}>
          We Give Entrepreneurs The Ability To Take Control Of Ready Made
          Startups Without Having To Do Any Of The Initial Product Development,
          Branding, Or Corporate Structuring.
        </div>
      </div>
      <div className={styles.coverRight}>
        <div className={styles.rightHeader}>Get A Free Consultation</div>
        <div className={styles.rightSubHeader}>
          Do You Already Have A Startup Idea?
        </div>
        <input type='text' className={styles.formInput} />
        <input type='text' className={styles.formInput} />
      </div> */}
    {/* </div> */}
    </>
  );
};

export default HomeCover;
