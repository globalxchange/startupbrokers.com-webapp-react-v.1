import styles from './feature.module.scss';

const HomeFeature = () => {
  return (
    <div className={styles.featureContainer}>
      <div className={styles.featureHeader}>Which Type Of Service Do You Need?</div>
      <div className={styles.featureMbHeader}>What Do You Need?</div>
      <div className={styles.featureList}>
        {window.innerWidth > 600 ? (
          <>
            {FEATURES.map((item) => (
              <div key={item.title} className={styles.featureItem}>
                <div className={styles.featureItemHeader}>
                  <img src={item.icon} alt='' className={styles.featureIcon} />
                  <div className={styles.featureTitle}>{item.title}</div>
                </div>
                <div className={styles.featureContent}>
                  <div className={styles.featuresItemList}>
                    {item.features.map((li) => (
                      <div key={li} className={styles.featureContentItem}>
                        <div className={styles.dot} />
                        {li}
                      </div>
                    ))}
                  </div>
                  <div className={styles.learnMore}>Learn More</div>
                </div>
              </div>
            ))}
          </>
        ) : (<>
          {MBFEATURES.map((item) => (
            <div key={item.title} className={styles.featureItem}>
              <div className={styles.featureItemHeader}>
                <img src={item.icon} alt='' className={styles.featureIcon} />
                <div className={styles.featureTitle}>{item.title}</div>
              </div>
              <div className={styles.featureContent}>
                <div className={styles.featuresItemList}>
                  {item.features.map((li) => (
                    <div key={li} className={styles.featureContentItem}>
                      <div className={styles.dot} />
                      {li}
                    </div>
                  ))}
                </div>
                <div className={styles.learnMore}>Learn More</div>
              </div>
            </div>
          ))}
        </>)}
      </div>
    </div>
  );
};

export default HomeFeature;

const FEATURES = [
  {
    title: 'Branding',
    icon: require('../../assets/images/homeFeature/coreBranding.svg').default,
    features: [
      'Premium Domain',
      'Logo/ Logo Animation/ Tagline',
      'Social Media Handles',
    ],
  },
  {
    title: 'Legal',
    icon: require('../../assets/images/homeFeature/legalAccounting.svg')
      .default,
    features: [
      'Incorporation',
      'Banking / Payments',
      'Contracts / Documentation',
    ],
  },
  {
    title: 'Software',
    icon: require('../../assets/images/homeFeature/productPrototype.svg')
      .default,
    features: [
      'Whitelabeled Applications',
      'Custom Frontend',
      'Custom Backend',
    ],
  },
];

const MBFEATURES = [
  {
    title: 'Legal',
    icon: require('../../assets/images/homeFeature/legalAccounting.svg')
      .default,
    features: [
      'Incorporation',
      'Banking / Payments',
      'Contracts / Documentation',
    ],
  },
  {
    title: 'Branding',
    icon: require('../../assets/images/homeFeature/coreBranding.svg').default,
    features: [
      'Premium Domain',
      'Logo/ Logo Animation/ Tagline',
      'Social Media Handles',
    ],
  },
  {
    title: 'Software',
    icon: require('../../assets/images/homeFeature/productPrototype.svg')
      .default,
    features: [
      'Whitelabeled Applications',
      'Custom Frontend',
      'Custom Backend',
    ],
  },
];
