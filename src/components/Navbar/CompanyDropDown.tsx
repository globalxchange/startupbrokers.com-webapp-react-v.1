import { VFC, useEffect } from 'react';
import styles from './navbar.module.scss';

type Props = {
  isDropDownOpen?: boolean;
  onClose: () => void;
};

const CompanyDropDown: VFC<Props> = ({ isDropDownOpen, onClose }) => {

  if(isDropDownOpen){
      document.body.style.overflow = "hidden";
  }else{
    document.body.style.overflow = "auto";
  }

  return (
    <div
      className={`${styles.dropdownContainer}  ${!isDropDownOpen && 'd-none'}`}
    >

      <div className={styles.maincompany}>
        <p className={styles.launch}>Launchpad</p>
        <div className={styles.companyList}>
          {COMPANIES.map((item) => (
            <div key={item.name} className={styles.companyItem}>
              <img src={item.icon} alt='' className={styles.companyImg} />
              <div className={styles.divider} />
              <div className={styles.companyType}>{item.name}</div>
            </div>
          ))}
          <div>
          </div>
        </div>
        <p className={styles.finance}>Financing</p>
        <div className={styles.companyList}>
          {THECOMPANIES.map((item) => (
            <div key={item.name} className={styles.companyItem}>
              <img src={item.icon} alt='' className={styles.companyImg} />
              <div className={styles.divider} />
              <div className={styles.companyType}>{item.name}</div>
            </div>
          ))}
        </div>
      </div>
      <div className={styles.overlay} onClick={onClose} />
    </div>
  );
};

export default CompanyDropDown;

const COMPANIES = [
  {
    title: 'Startup Broker',
    name: 'CONSULTING',
    icon: require('../../assets/images/companyLogos/startupbroker.svg').default,
  },
  {
    title: 'Decks',
    name: 'PITCH',
    icon: require('../../assets/images/companyLogos/decks.svg').default,
  },
  {
    title: 'MeatverseApps',
    name: 'APP STORE',
    icon: require('../../assets/images/companyLogos/metaverseApps.svg').default,
  },
  {
    title: 'Startup Broker',
    name: 'MARKETPLACE',
    icon: require('../../assets/images/companyLogos/bos.svg')
      .default,
  }
];


const THECOMPANIES = [
  {
    title: 'Capitalized',
    name: 'EQUITY',
    icon: require('../../assets/images/companyLogos/capitilized.svg').default,
  },
  {
    title: 'Banker',
    name: 'DEBT',
    icon: require('../../assets/images/companyLogos/banker.svg').default,
  },
  {
    title: 'funnel',
    name: 'INVESTOR CRM',
    icon: require('../../assets/images/companyLogos/funnel.svg').default,
  },
  {
    title: 'Investor data',
    name: 'INVESTOR DATA',
    icon: require('../../assets/images/companyLogos/investorSocial.svg')
      .default,
  },
]