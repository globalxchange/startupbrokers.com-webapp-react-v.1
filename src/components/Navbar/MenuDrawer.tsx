import { useState, VFC } from 'react';
import { FiSearch } from 'react-icons/fi';
import styles from './navbar.module.scss';

type Props = { isMenuOpened?: boolean };

const MenuDrawer: VFC<Props> = ({ isMenuOpened }) => {
  const [activeMenu, setActiveMenu] = useState(MENU_ITEMS[0]);

  return (
    <aside
      className={[styles.navContainer, isMenuOpened && styles.navOpened].join(
        ' '
      )}
      role='menu'
    >
      <div className={styles.menuHeader}>All Categories</div>
      <div className={styles.searchContainer}>
        <FiSearch />
        <input
          type='text'
          className={styles.search}
          placeholder='Start Typing To Search'
        />
      </div>
      <nav>
        <ul>
          {MENU_ITEMS.map((item: any) => (
            <li
              key={item.title}
              onClick={() => setActiveMenu(item)}
              className={activeMenu.title === item.title ? styles.active : ''}
            >
              <img src={item.icon} alt='' />
              <div className={styles.menuItemTitle}>{item.title}</div>
            </li>
          ))}
        </ul>
      </nav>
    </aside>
  );
};

export default MenuDrawer;

const MENU_ITEMS = [
  {
    title: 'Fin-Tech',
    icon: require('../../assets/images/menuIcons/fintecIcon.svg').default,
  },
  {
    title: 'Capital Markets',
    icon: require('../../assets/images/menuIcons/capitalMarket.svg').default,
  },
  {
    title: 'E-commerce',
    icon: require('../../assets/images/menuIcons/eCommerce.svg').default,
  },
  {
    title: 'Content',
    icon: require('../../assets/images/menuIcons/content.svg').default,
  },
  {
    title: 'Marketing',
    icon: require('../../assets/images/menuIcons/marketing.svg').default,
  },
  {
    title: 'Communications',
    icon: require('../../assets/images/menuIcons/communications.svg').default,
  },
];
