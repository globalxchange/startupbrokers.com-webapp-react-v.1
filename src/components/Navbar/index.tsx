import styles from './navbar.module.scss';
import { FiChevronDown, FiMenu, FiX, FiSearch } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MenuDrawer from './MenuDrawer';
import CompanyDropDown from './CompanyDropDown';
import BOSimg from "../../assets/images/companyLogos/bosimg.png"
import { useLocation, useNavigate } from "react-router-dom";
import "./navbar.scss";
import BrokerIcon from "../../assets/images/startupbrokers.svg"

const Navbar = (active:any) => {
  active = "About"
  const [isMenuOpened, setIsMenuOpened] = useState(false);
  const [isDropDownOpen, setIsDropDownOpen] = useState(false);

  const toggleMenu = () => setIsMenuOpened((prevState) => !prevState);

  const toggleDropdown = () => setIsDropDownOpen((prevState) => !prevState);

  const navigate = useNavigate();


  useEffect(() => {
    document.body.style.overflowX = "hidden";
  }, [])

  const navs = [{
    "menu": "About"
  },
  {
    "menu": "Instruments"
  },
  {
    "menu": "Markets"
  },
  {
    "menu": "Solutions"
  },
  {
    "menu": "Login"
  },
  {
    "menu": "Register"
  }]

  const opentab = (e:any) => {
    // console.log(e.target.innerText)
    if(e.target.innerText === "About"){
      navigate("/")
    }
    // if(e.target.innerText === "Markets"){
    //   navigate("/markets")
    // }
  }

  return (
    <>
      <>
      <div className="navbar1">
        {/* <hr className="the-hrtag" /> */}
        {/* <img src={DashLines} alt="dash-lines" /> */}
        <img src={BrokerIcon} alt="funnel-icon" className="funnel-icon" />
        <div className="vl"></div>
        <input type="text" className="search-field" placeholder="Search ShareTokens..." />
        {/* <div className="vl"></div> */}
        <div className="menu">
          <div className="vl"></div>
          {navs.map(element => {
            let bgm
            let thevl
            if (element.menu === "Login") {
              thevl = "none"
            }
            if (element.menu === "Register") {
              bgm = "register-bg"
              thevl = "none"
            }

            return (
              <>
                <p className={"menu-item " + bgm + (active === element.menu ? " active underline" : " ")} onClick = {opentab}>{element.menu}</p>
                <div className="vl2" style={{ display: thevl }}></div>
              </>
            )
          })}
        </div>
      </div>
    </>
      {/* <div className={styles.navbar}>
        <div className={styles.navbarDetails}>
          <div className={styles.menuIcon} onClick={toggleMenu}>
            {isMenuOpened ? <FiX size={30} /> : <FiMenu size={30} />}
          </div>
          <Link to='/' className={styles.mainlink}>
            <img
              src={require('../../assets/images/startupBrokerIcon.svg').default}
              alt=''
              className={styles.headerLogo}
            />
          </Link>
          <div className={styles.separator} />
          <div className={styles.links}>
          <a
            href='https://nvest.com/'
            target='_blank'
            rel='noreferrer'
            className={styles.brandChange}
          >
            BY <span><img src = {BOSimg} alt = "bos-img"/></span>
          </a>
          </div>
          <div className={styles.dropDownBtn} onClick={toggleDropdown}>
            {isDropDownOpen ? <FiX size={23} /> : <FiChevronDown size={23} />}
          </div>
        </div>
        <div className={styles.headerSearch}>
          <input
            type='text'
            className={styles.search}
            placeholder='Search For Startups....'
          />
          <FiSearch color='grey' size={20} className = {styles.searchicon}/>
        </div>
        <div className={styles.headerActions}>
          <div className={styles.filledBtn}>Login</div>
          <div className={styles.separator} />
          <div className={styles.outlineBtn}>Create</div>
        </div>
        <MenuDrawer isMenuOpened={isMenuOpened} />
      </div>
      <div className={styles.thecomp}>
      <CompanyDropDown
        isDropDownOpen={isDropDownOpen}
        onClose={() => setIsDropDownOpen(false)}
      />
      </div> */}
    </>
  );
};

export default Navbar;
