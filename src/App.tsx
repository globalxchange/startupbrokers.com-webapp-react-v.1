import { Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ToastContainer } from 'react-toastify';
import HomePage from './pages/HomePages';
import MainContextProvider from "../src/pages/Context/context"

const queryClient = new QueryClient();

const App = () => {
  return (
    <MainContextProvider>
      <QueryClientProvider client={queryClient}>
        <Routes>
          {/* <Route path='/directory' element={<DirectoryPage />} />
        <Route path='/:brandId' element={<BrandPage />} /> */}
          <Route path='/' element={<HomePage />} />
        </Routes>
        <ToastContainer autoClose={3000} />
      </QueryClientProvider>
    </MainContextProvider>
  );
};

export default App;
